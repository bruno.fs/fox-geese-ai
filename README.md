## fox-geese-ai {#mainpage}
AI player for the fox and geese game.

### Initialize submodules
If you clone this repository call this to clone the dependencies

`git submodule update --recursive --init;`

Will create a folder ./Grimoire, it will be its own git repository.

Update submodules with

`git submodule update --recursive`

Implementação C++ de uma busca minimax corte alpha–beta do jogo "A raposa e os gansos".

Para executar:
$ ./fox-geese-ai r [ip porta]  # executa como raposa
$ ./fox-geese-ai g [ip porta]  # executa como gansos
   ip: parâmetro opcional que indica o ip ou o hostname do servidor redis
      o valor default é 127.0.0.1
   porta: parâmetro opcional que indica a porta do servidor redis
      o valor default é 10001
	maxdepth: profundidade máxima da árvore de busca;
      o valor default é 6

Na busca minimax os gansos são maximizadores e a raposa é minimizadora. Escrevemos o
código desta forma para evitar ter funções repetidas no código.

A heuristica para avaliar o tabuleiro (Board::Eval()) é:

+ O numero de gansos vivos.

+ O numero espaços vazios que a área em volta da raposa tem, contado com flood fill.

      Por exemplo:
      "         "
      "   ggg   "
      "   ggg   "
      " ggggggg "
      " ------- "
      " ---r--- "
      "   ---   "
      "   ---   "
      // a raposa tem 19 de freeSpace

      "         "
      "   ggg   "
      "   ggg   "
      " --gggg- "
      " gg----g "
      " ---r--- "
      "   ---   "
      "   ---   "
      // a raposa tem 16 de freeSpace

A profundidade máxima da árvore minimax é um parâmetro, e por padrão é 6.

Os movimentos dos gansos são explorados de forma a mover gansos mais para baixo do board primeiro para maximixar o corte alpha-beta.
Supomos que movimentos de gansos mais para baixo tem maior probabilidade de serem jogadas boas, pelo menos na maioria das jogadas
iniciais.
Pelo mesmo motivo a raposa explora primeiro movimentos de pulo, caso eles existam.

A class Board armazena movimentos feitos com a chamada de move(), para evitar a copia de boards na ramificação da árvore.

Por exemplo, board.move(Coord{3,1}, Coord{4,1},piece::goose) movimenta um ganso de 3,1 para 4,1 e armazena o movimento em uma lista.

Na volta da recursão, desfazemos o movimento com board.undo().

O tabuleiro é apenas copiado para fazer o calculo do freeSpace flood fill nos nodos folhas.


--------------------------------------------------------------------------------------------------------------------------------------------
O programa pode criar a árvore de busca em formato de grafo '.dot' do Graphviz. O arquivo estará no local de execução "./graph.dot".
Cada turno acrescenta ao arquivo "digraph turnNumber { ... }".
Para observar a árvore de um turno copie a secção digraph desejada para outro arquivo 'filename.dot' e chame "dot -Tpng filename.dot -O"

WARNING: o arquivo cresce rapidamente de tamanho com grandes MAXDEPTH em minimax. Espere 1GB apos 50 turnos, aproximadamente.

Para ativar:

1. modificar a seguinte linha em src/main.cpp:

         gm::LogLine<gm::LogLvl::Warn> graphLine(graphStream);
      para:
         gm::LogLine<gm::LogLvl::Debug> graphLine(graphStream);

2. modificar a seguinte linha em include/streams.hpp:

         extern gm::LogLine<gm::LogLvl::Warn> graphLine;
      para:
         extern gm::LogLine<gm::LogLvl::Debug> graphLine;
--------------------------------------------------------------------------------------------------------------------------------------------


A class Move tem 2 tipos, o movimento simple e o pulo (jump):
   movimentos simples têm move.jumps.size() == 0.
   jumps tem Coord endCoord inválida, o vetor jumps é utilizado pra isso.
   jumps é a sequencia de coordenadas como definida na especificação (specs/spec.txt) (exeto a coordenada inicial)
   jumps também não podem ter move.piece == piece::goose.