/**
@mainpage

Description

This program plays a game of Fox and Geese.
It interfaces with a game controller to play the game against another player.
*/
#include <string>

#include "Minimax.hpp"

#include "Logger.hpp"
#include "streams.hpp"

extern "C" void redis_connect(int, char **);
extern "C" void redis_send(char *, char);
extern "C" void redis_receive(char *, char);

#include "BoardTests.hpp"