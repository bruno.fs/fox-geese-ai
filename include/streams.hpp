#pragma once

#include "Logger.hpp"

extern gm::LogLine<gm::LogLvl::Warn> graphLine;
extern gm::LogLine<gm::LogLvl::Info> logLine;
