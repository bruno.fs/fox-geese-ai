
#include <cassert>

#include "Board.hpp"

void test_freeSpaceCount()
{
   Board board;

   strcpy(board.map[0], "         ");
   strcpy(board.map[1], "   ggg   ");
   strcpy(board.map[2], "   ggg   ");
   strcpy(board.map[3], " ggggggg ");
   strcpy(board.map[4], " ------- ");
   strcpy(board.map[5], " ---r--- ");
   strcpy(board.map[6], "   ---   ");
   strcpy(board.map[7], "   ---   ");

   assert(board.freeSpaceCount(Coord{5, 4}) == 7 * 2 - 1 + 3 * 2);

   strcpy(board.map[0], "         ");
   strcpy(board.map[1], "   ggg   ");
   strcpy(board.map[2], "   ggg   ");
   strcpy(board.map[3], " --gggg- ");
   strcpy(board.map[4], " gg----g ");
   strcpy(board.map[5], " ---r--- ");
   strcpy(board.map[6], "   ---   ");
   strcpy(board.map[7], "   ---   ");
   assert(board.freeSpaceCount(Coord{5, 4}) == 7 * 2 - 1 + 3 * 2 - 3);

   strcpy(board.map[0], "         ");
   strcpy(board.map[1], "   grg   ");
   strcpy(board.map[2], "   ggg   ");
   strcpy(board.map[3], " ggggggg ");
   strcpy(board.map[4], " ------- ");
   strcpy(board.map[5], " ------- ");
   strcpy(board.map[6], "   ---   ");
   strcpy(board.map[7], "   ---   ");
   assert(board.freeSpaceCount(Coord{1, 4}) == 0);

   strcpy(board.map[0], "         ");
   strcpy(board.map[1], "   ggg   ");
   strcpy(board.map[2], "   --g   ");
   strcpy(board.map[3], " ------- ");
   strcpy(board.map[4], " ------- ");
   strcpy(board.map[5], " ---r--- ");
   strcpy(board.map[6], "   ---   ");
   strcpy(board.map[7], "   ---   ");
   assert(board.freeSpaceCount(Coord{5, 4}) == Board::freeSpaceMax);
   board.foxPos_ = Coord{5, 4};
   board.geeseNo_ = 4;
   board.move(Coord{5, 4}, Coord{4, 4}, piece::fox);
   assert(board.freeSpaceCount(Coord{4, 4}) == Board::freeSpaceMax);
}

void test_foxPos()
{
	Board board;

	strcpy(board.map[0], "         ");
	strcpy(board.map[1], "   ggg   ");
	strcpy(board.map[2], "   ggg   ");
	strcpy(board.map[3], " ggggggg ");
	strcpy(board.map[4], " ------- ");
	strcpy(board.map[5], " ---r--- ");
	strcpy(board.map[6], "   ---   ");
	strcpy(board.map[7], "   ---   ");

	assert(board.freeSpaceCount(Coord{5, 4}) == Board::freeSpaceMax);
	board.foxPos_ = Coord{5, 4};
	board.geeseNo_ = 4;
	board.move(Coord{5, 4}, Coord{4, 4}, piece::fox);
	assert(board.freeSpaceCount(Coord{4, 4}) == Board::freeSpaceMax);
}