#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>  // abs 
#include <algorithm> // std::max
#include <climits>	// INT_MIN, INT_MAX
#include <type_traits> // std::is_same<>

#include "Coord.hpp"
#include "Move.hpp"
#include "Board.hpp"
#include "streams.hpp"

extern int MAXDEPTH;

#define MINUSINF INT_MIN //< Minimax leaf values
#define PLUSINF INT_MAX

class MaxPlayer
{
public:
	static char tochar(){return piece::goose;};
	static char opponentchar();
};

class MinPlayer
{
public:
	static char tochar(){return piece::fox;};
	static char opponentchar();
};

class MoveUtility
{
public:
	Move move;
	int utility;

	MoveUtility(){};

	MoveUtility(int utility)
		 : utility(utility) {};

	void update(const Move & move, const int & utility)
	{
		this->move = move;
		this->utility = utility;
	}
};

/**
 * @brief helper struct to make a move on the board and auto undo when created object goes out of scope
 * makes move in the board at construction, undos last move at destruction
 */
struct MoveAutoUndo{
	Board & board;
	/**
	 * @brief Makes a move
	 */
	MoveAutoUndo(Board & board, const Move & move) : board(board)
	{
		board.move(move);
	}
	/**
	 * @brief Undos last move made
	 */
	~MoveAutoUndo()
	{
		board.undo();
	}
};

template <typename playerType>
bool minimaxMove(Board &board, int &alpha, int &beta, const int &depth, const Move &mov, MoveUtility &bestMove);

template <typename playerType>
MoveUtility minimax(Board &board, int alpha, int beta, int depth);

/**
 * @brief Goes down this move's branch and updates {alpha, beta, bestMove}
 * calls minimax for the next player
 * 
 * @tparam playerType who is making the play, MinPlayer or MaxPlayer
 * @param board before mov
 * @param alpha will be updated
 * @param beta will be updated
 * @param depth 
 * @param mov to be applied
 * @param bestMove will be updated if mov is better than bestMove (by calling minimax)
 * @return true if Alpha–beta pruning cutoff occured (the caller of this func should return bestMove)
 * @return false if you should continue evaluating others moves in your current node
 */
template <typename playerType>
inline bool minimaxMove(Board &board, int &alpha, int &beta, const int &depth, const Move &mov, MoveUtility &bestMove)
{
	graphLine.msg(gm::LogLvl::Debug) << board.dotName(depth) << " -> ";


	// Make board move (auto undo when movehelper is out of scope)
	MoveAutoUndo movehelper(board, mov);


	graphLine.msg(gm::LogLvl::Debug) << board.dotName(depth+1) << ";\n"; graphLine.flush();

	MoveUtility moveBranch;

	// Calculate minimax val for this branch and update bestValue
	if (std::is_same<playerType, MaxPlayer>::value) {
		moveBranch = minimax<MinPlayer>(board, alpha, beta, depth + 1);
	} else {
		moveBranch = minimax<MaxPlayer>(board, alpha, beta, depth + 1);
	}
	if (moveBranch.move.isDefault()) { // the branch reached board endgame
		graphLine.msg(gm::LogLvl::Debug)
			 << board.dotName(depth + 1) << " -> "
			 << "\"" << depth + 1 << "\\lUtl=" << moveBranch.utility << "\\l" << board.dotBoard() << "\";\n";
		graphLine.flush();
	}


	// if you found that mov gives a better utility, it's now your best move
	if ((std::is_same<playerType, MaxPlayer>::value && // Maximizer
		  moveBranch.utility > bestMove.utility) ||
		 (std::is_same<playerType, MinPlayer>::value && // Minimizer
		  moveBranch.utility < bestMove.utility))
	{
		bestMove.update(mov, moveBranch.utility);

		// Update alpha/beta (current best possible play for the players)
		if (std::is_same<playerType, MaxPlayer>::value)
		{
			// alpha = std::max(alpha, bestMove.utility);
			alpha = bestMove.utility;
		}
		else if (std::is_same<playerType, MinPlayer>::value)
		{
			// beta = std::min(beta, bestMove.utility);
			beta = bestMove.utility;
		}

		// Pruning
		if (alpha >= beta)
		{
			return true;
		}
	}
	return false;
}

template <typename playerType>
MoveUtility minimax(Board &board, int alpha, int beta, int depth)
{
	if (depth == MAXDEPTH){
		return MoveUtility(board.Eval());
	}
	int utility;
	if ((utility = board.EndGame()))
		return MoveUtility(utility);
	/* 
   if(geese player)
      for(each goose)
         for(each valid move for this goose)
            recursive minimax
				if(alpha-beta cutoff) return
	else if(fox player)
      find fox
		for(each valid jump move for fox)
			recursive minimax
			if(alpha-beta cutoff) return
		for(each valid simple move for fox)
			recursive minimax
			if(alpha-beta cutoff) return
   */
	// Maximizer, goose player
	if (std::is_same<playerType, MaxPlayer>::value)
	{
		MoveUtility bestMove(MINUSINF); // Initialize with worst case scenario
		// For every goose
		for (int i = 7; i >= 1; --i) { // Every line, from down to up because of alphabeta cutoff
		int j = 1; int endj = 7;
    	if (i<3 || i>5) { j=3; endj = 5; } // skip out of bounds columns
		for (; j <= endj; ++j) { // Every column
		if (board.map[i][j] == piece::goose)
		{
			// for valid moves : (-1, 0), (0,-1), (0,1), (1,0)
			for (int e = -1; e <= 1; ++e) {
			for (int r = -1; r <= 1; ++r) {
			if (std::abs(e + r) == 1){
				Coord dir(e,r);
				// if direction is free to move
				if (board.available(i + dir.i, j + dir.j))
				{
					Coord ini(i, j); // initial position
					Coord end(i + dir.i, j + dir.j); // end position
					Move mov(ini, end, piece::goose);
					// recursive minimax if(alpha-beta cutoff) return
					bool cutoff = minimaxMove<MaxPlayer>(board, alpha, beta, depth, mov, bestMove);
					if(cutoff)
						return bestMove;
				}
			}}}
		}}}
		return bestMove;
	}
	// Minimizer, fox player
	else if (std::is_same<playerType, MinPlayer>::value)
	{
		MoveUtility bestMove(PLUSINF);
		// Find fox
		Coord foxPos = board.foxPos();
		//<<< For each jump move
		// calculate all possible moves and put them in 'moves'
		std::vector<Move> moves;
		std::vector<Coord> jumps; // auxiliar variable
		possibleJumps(board, foxPos, jumps, moves);
		// for each jump possible
		for (const auto &mov : moves) {
			// recursive minimax if(alpha-beta cutoff) return
			bool cutoff = minimaxMove<MinPlayer>(board, alpha, beta, depth, mov, bestMove);
			if (cutoff)
				return bestMove;
		}
		//<<< For each simple move
		// for valid moves : (-1, 0), (0,-1), (0,1), (1,0)
		for (int e = -1; e <= 1; ++e) {
		for (int r = -1; r <= 1; ++r) {
		if (std::abs(e + r) == 1){
			Coord dir(e, r);
			// if direction is free to move
			if (board.available(Coord(foxPos.i + dir.i, foxPos.j + dir.j)))
			{
				// foxPos == initial position // Find fox
				Coord end(foxPos.i + dir.i, foxPos.j + dir.j); // end position
				Move mov(foxPos, end, piece::fox);
				// recursive minimax if(alpha-beta cutoff) return
				bool cutoff = minimaxMove<MinPlayer>(board, alpha, beta, depth, mov, bestMove);
				if (cutoff)
					return bestMove;
			}
		}}}
		return bestMove;
	}
}
