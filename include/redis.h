#pragma once

#include <hiredis/hiredis.h>

void redis_connect(int argc, char **argv);
void redis_send(char *buffer, char player_side);
void redis_receive(char *buffer, char player_side);