#pragma once

#include <vector>

#include "Coord.hpp"

class piece
{
public:
	static const char goose = 'g';
	static const char fox   = 'r';
	static const char empty = '-';
	static const char oob = ' ';
	static const char freeSpaceAux = 's';
};

/* stores information about a move from geese or fox
 */
class Move
{
public:
	Coord iniCoord = Coord(0, 0);	//< initial position
	Coord endCoord = Coord(0, 0); //< end position. only meaningful if it's a simple move (not jump)
	char piece;	//< moved piece {piece::fox, piece::goose}
	std::vector<Coord> jumps;	//< all jumps

	Move()
	{
	}
	
	Move(const char &piece) : piece(piece)
	{
	}

	Move(const Coord &initialCoord, const Coord &lastCoord, const char &piece)
		 : iniCoord(initialCoord), endCoord(lastCoord), piece(piece)
	{
	}

	Move(const Coord &initialCoord, const std::vector<Coord> &jumps)
		 : iniCoord(initialCoord), piece(piece::fox), jumps(jumps)
	{
	}

	bool isDefault(){
		 return iniCoord.i == 0;
	}

	/*      i0 j0 i1 j1
		r m   5  3  5  4
		g m   3  5  4  5
		n     i0 j0  i1 j1  i2 j2
		r s 3 (5  3) (3  3) (3  5) */
	std::string tostring() const
	{
		std::ostringstream outStream;

		if (jumps.empty())
		{
			outStream << piece << " m " << iniCoord << " " << endCoord;
		}
		else
		{
			outStream << piece << " s " << (jumps.size() + 1) << " " << iniCoord;
			for (const Coord &jump : jumps)
			{
				outStream << " " << jump;
			}
		}

		return outStream.str();
	}
};

std::ostream &operator<<(std::ostream &os, const Move &obj);