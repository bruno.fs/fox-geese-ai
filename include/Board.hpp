#pragma once

#include <iostream>
#include <sstream>
#include <vector>
#include <stack>
#include <string>
#include <stdlib.h>
#include <stdexcept>
#include <climits>
#include <cstring>

#include "Coord.hpp"
#include "Move.hpp"

extern char EVAL_USED;

class Board
{
public:
	const static int  MAX_J = 9;
	const static int  MAX_I = 9;

	const static int freeSpaceMax = 28; // 3*4+7*3-1-4 (-1 fox, -4 min geese alive)

	char map[MAX_I][MAX_J + 1] =
		{
			"         ",
			"   ggg   ",
			"   ggg   ",
			" ggggggg ",
			" ------- ",
			" ---r--- ",
			"   ---   ",
			"   ---   ",
			"         "
		};

	char freeSpace[MAX_I][MAX_J + 1]; //< freeSpaceCount auxiliar map

	Coord foxPos_ = Coord{5,4};
	int geeseNo_ = 7+3*2;
	std::vector<Move> moves;

	Board()
	{
	}

	char &at(const Coord &pos)
	{
		return map[pos.i][pos.j];
	}

	char &at(const int &i, const int &j)
	{
		return at(Coord(i, j));
	}

	char &freeSpaceAt(const Coord &pos)
	{
		return freeSpace[pos.i][pos.j];
	}

	/**
	 * @brief If board position is in bounds
	 */
	bool isValid(const Coord &pos)
	{
		return at(pos) != piece::oob;
	}
	
	bool isValid(const int &i, const int &j)
	{
		return isValid(Coord(i, j));
	}
	/**
	 * @brief If board position can be moved (simply, not jumped)
	 */
	bool available(const Coord &pos)
	{
		return at(pos) == piece::empty;
	}

	bool available(const int &i, const int &j)
	{
		return available(Coord(i,j));
	}

	int blockedNo(const int &i, const int &j)
	{
		int no = 4; // assume all moves blockes
		// find open moves
		// for valid moves : (-1, 0), (0,-1), (0,1), (1,0)
		for (int e = -1; e <= 1; ++e) {
		for (int r = -1; r <= 1; ++r) {
		if (std::abs(e + r) == 1){
		  Coord dir(e,r);
			if (((at(i + dir.i, j + dir.j) == piece::goose) && // jump
					(at(i + 2 * dir.i, j + 2 * dir.j) == piece::empty)) ||
					available(i + dir.i, j + dir.j)) // or available
			{
				--no; // space is actually available
			}
		}}}
		return no;
	}

	bool blocked(const int &i, const int &j)
	{
		// find open moves
		// for valid moves : (-1, 0), (0,-1), (0,1), (1,0)
		for (int e = -1; e <= 1; ++e) {
		for (int r = -1; r <= 1; ++r) {
		if (std::abs(e + r) == 1){
			Coord dir(e,r);
			if (((at(i + dir.i, j + dir.j) == piece::goose) && // Jump over 'g'
				  (at(i + 2 * dir.i, j + 2 * dir.j) == piece::empty)) ||
				 available(i + dir.i, j + dir.j)) // or available
			{
				return false; // has at least one move
			}
		}}}
		return true;
	}

	Coord foxPos()
	{
		return foxPos_;
	}

	void move(const Coord &ini, const Coord &end, const char &pieceMoved)
	{
		at(ini) = piece::empty;
		at(end) = pieceMoved;
		moves.emplace_back(ini, end, pieceMoved);
		// Update fox position
		if (pieceMoved == piece::fox) {
			foxPos_ = end;
		}
	}

	void move(const Coord &ini, const std::vector<Coord> &jumps)
	{
		at(ini) = piece::empty; // it's jumping
		Coord prevPos = ini; // starting postition of the fox
		for (size_t j = 0; j < jumps.size(); ++j)
		{
			const Coord &dest = jumps[j]; // destination is current Coord
			at(between(prevPos, dest)) = piece::empty; // Kill goose
			prevPos = dest;	// fox is now at destination
		}
		// put fox at the end of the jumps
		at(jumps.back()) = piece::fox;
		// record move
		moves.emplace_back(ini, jumps);
		// Update fox position
		foxPos_ = jumps.back(); // to end of jump
		// Update geeseNo
		geeseNo_ -= jumps.size(); // Kill geese
	}

	void move(const Move & mov)
	{
		if (mov.jumps.empty())
		{
			move(mov.iniCoord, mov.endCoord, mov.piece);
		} else
		{
			move(mov.iniCoord, mov.jumps);
		}
	}

	/**
	 * @brief Undos last move
	 * @return true if last move was undone
	 * @return false when no move exists
	 */
	bool undo()
	{
		if (moves.empty())
			return false;

		const Move &mov = moves.back();
		// if simple move
		if (mov.jumps.empty())
		{
			at(mov.iniCoord) = mov.piece;
			at(mov.endCoord) = piece::empty;
			// Update fox position
			if (mov.piece == piece::fox) {
				foxPos_ = mov.iniCoord;
			}
		}
		else // if jump move
		{
			at(mov.jumps.back()) = piece::empty;
			Coord prevPos = mov.iniCoord;
			for (size_t j = 0; j < mov.jumps.size(); ++j)
			{
				const Coord &dest = mov.jumps[j]; // destination is current Coord
				// Revive geese
				at(between(prevPos, dest)) = piece::goose;
				prevPos = dest;
			}
			at(mov.iniCoord) = piece::fox;

			// Update fox position
			foxPos_ = mov.iniCoord;
			// Update geeseNo
			geeseNo_ += mov.jumps.size(); // Revive geese
		}
		moves.pop_back();
		return true;
	}

	inline void cpymap(char dst[MAX_I][MAX_J + 1], char src[MAX_I][MAX_J + 1])
	{
		for (int i = 1; i < MAX_I - 1; ++i)
			strcpy(dst[i], src[i]);
	}
	/**
	 * @brief if target is a empty space not counted yet
	 * returns value to be added to freeSpaceNo and adds to stack
	 */
	inline int fill(const Coord &target, std::stack<Coord> &mystack)
	{
		// empty space
		if (freeSpaceAt(target) == piece::empty)
		{
			// mark as counted
			freeSpaceAt(target) = piece::freeSpaceAux;
			// put on stack to see their neighbours later
			mystack.push(target);
			// return value to be added into count
			return 1;
		}
		else /* not empty space */
		{
			return 0;
		}
	}
	/**
	 * @brief Counts the amount of 'free space' the coordinates have
	 * Free space being the number of adjacent empty spaces, which means the number of
	 * places you could move around (without performing a jump move)
	 * @return int amount of adjacent free space  */
	int freeSpaceCount(const Coord & position)
	{
		int freeSpaceNo = 0;
		std::stack<Coord> mystack;
		mystack.push(position);

		cpymap(freeSpace, map);

		while (!mystack.empty())
		{
			Coord source = mystack.top();
			mystack.pop();

			if (source.i > 0) { // up
				freeSpaceNo += fill(Coord{source.i - 1, source.j}, mystack);
			}
			if (source.i < MAX_I - 1) { // down
				freeSpaceNo += fill(Coord{source.i + 1, source.j}, mystack);
			}
			if (source.j > 0) { // left
				freeSpaceNo += fill(Coord{source.i, source.j - 1}, mystack);
			}
			if (source.j < MAX_J - 1) { // right
				freeSpaceNo += fill(Coord{source.i, source.j + 1}, mystack);
			}
		}
		return freeSpaceNo;
	}

	int squaredManhattanSum()
	{
		int sum = 0;
		int gn = 0;

		for(int i = 1; i <= 7; i++)
			for(int j = 1; j <= 7; j++)
				if(map[i][j] == piece::goose)
				{
					sum += (i - foxPos_.i) * (i - foxPos_.i) + (j - foxPos_.j)  * (j - foxPos_.j);
					gn++;
				}

		sum += 40 * ((13-gn));

		return sum;
	}

	/**
	 * @brief Calculates utility value of the board
	 * @return int utility  */
	int Eval()
	{
		// Fox win
		if (geeseNo_ < 4) {
			return INT_MIN;
		}
		int freeSpaceNo = freeSpaceCount(foxPos_);
		// Geese win
		if(freeSpaceNo == 0) {
			return INT_MAX;
		}

		if(EVAL_USED == 'a')
			return geeseNo_ + 2*(freeSpaceMax - freeSpaceNo);
		else
			return (-1 * squaredManhattanSum());
	}

	/*
	int Eval()
	{
		// Fox win
		if (geeseNo_ < 4) {
			return INT_MIN;
		}
		int freeSpaceNo = freeSpaceCount(foxPos_);
		// Geese win
		if(freeSpaceNo == 0) {
			return INT_MAX;
		}
		return geeseNo_ + 2*(freeSpaceMax - freeSpaceNo);
	}*/

	int EndGame()
	{
		// Fox wins
		if (geeseNo_ < 4) {
			return INT_MIN;
		}
		// Geese win
		if (blocked(foxPos_.i, foxPos_.j))
			return INT_MAX;
		// Not endgame
		return 0;
	}

	std::string dotBoard() {
		std::ostringstream os;
		for (int i = 0; i < MAX_I; ++i) {
			os << map[i] << "\\l";
		}
		return os.str();
	}

	std::string dotName(const int &depth) {
		std::ostringstream os;
		os << "\"" << depth << "\\l";
		os << dotBoard();
		os << "\"";
		return os.str();
	}
};

void possibleJumps(Board &board, const Coord &ini, std::vector<Coord> &jumps, std::vector<Move> &moves);

std::ostream &operator<<(std::ostream &os, const Board &obj);
