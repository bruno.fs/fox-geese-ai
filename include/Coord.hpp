#pragma once

#include <sstream>

/* Coordinate class
 */
class Coord
{
 public:
	int i, j;

	Coord()
		 : i(0), j(0)
	{
	}

	Coord(int horizontal, int vertical)
		 : i(horizontal), j(vertical)
	{
	}
};

Coord between(const Coord &start, const Coord &dest);

std::ostream &operator<<(std::ostream &os, const Coord &obj);