
#include "Coord.hpp"

Coord between(const Coord &start, const Coord &dest)
{
	return Coord((start.i + dest.i) / 2, (start.j + dest.j) / 2);
}

std::ostream &operator<<(std::ostream &os, const Coord &obj)
{
	os << obj.i << " " << obj.j;
	return os;
}