#include <stdlib.h>
#include <string.h>
#include <hiredis/hiredis.h>
#include "redis.h"

redisContext *rediscontext;
redisReply *redisreply;

char key[16];

void redis_connect(int argc, char **argv)
{
  const char *ip;
  int port;

  if(argc < 2) {
    printf("formato:\n");
    printf("         %s lado [ip porta]\n\n", argv[0]);
    printf("   lado: indica com que peças jogar, os valores possívies são r ou g\n");
    printf("   ip: parâmetro opcional que indica o ip ou o hostname do servidor redis\n");
    printf("       o valor default é 127.0.0.1\n");
    printf("   porta: parâmetro opcional que indica a porta do servidor redis\n");
    printf("          o valor default é 10001\n");
    exit(1);
  }

  ip  = (argc > 2) ? argv[2] : "127.0.0.1";
  port = (argc > 3) ? atoi(argv[3]) : 10001;

  rediscontext = redisConnect(ip, port);

  if (rediscontext == NULL || rediscontext->err) {
    if(rediscontext) {
      printf("Erro ao conectar com o servidor redis: %s\n", rediscontext->errstr);
    } else {
      printf("Não foi possível conectar com o servidor redis\n");
    }
    exit(1);
  }

  printf("Successfully connected to redis.\n");
}

void redis_send(char *buffer, char player_side)
{
  sprintf(key, "jogada_%c", player_side);
  redisreply = (redisReply *)redisCommand(rediscontext, "RPUSH %s %s", key, buffer);
  freeReplyObject(redisreply);
  printf("> redis_sent\n");
}

void redis_receive(char *buffer, char player_side)
{
  sprintf(key, "tabuleiro_%c", player_side);
  redisreply = (redisReply *) redisCommand(rediscontext, "BLPOP %s 0", key);
  strcpy(buffer, redisreply->element[1]->str);
  freeReplyObject(redisreply);
  printf("> redis_received\n");
}