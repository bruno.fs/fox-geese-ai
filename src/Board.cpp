#include "Board.hpp"

void possibleJumps(Board &board, const Coord &ini, std::vector<Coord> &jumps, std::vector<Move> &moves)
{
	Coord pos;
	if (!jumps.empty())
		pos = jumps.back();
	else
		pos = ini;
	int i = pos.i;
	int j = pos.j;
	for (const Coord & dir : {Coord{-1, 0},Coord{0,-1},Coord{0,1},Coord{1,0}}) {
		if ((board.map[i + dir.i][j + dir.j] == piece::goose) && // Jump over 'g'
			 (board.map[i + 2 * dir.i][j + 2 * dir.j] == piece::empty))
		{
			Coord dest(i + 2 * dir.i, j + 2 * dir.j);
			jumps.push_back(dest);
			board.move(pos, std::vector<Coord>({dest}));
			possibleJumps(board, ini, jumps, moves);
			jumps.pop_back();
			board.undo();
		}
	}

	if (!jumps.empty())
		moves.emplace_back(ini, jumps);
}

std::ostream &operator<<(std::ostream &os, const Board &obj)
{
	for (int i = 0; i < Board::MAX_I; ++i)
	{
		os << obj.map[i] << "\n";
	}
	return os;
}