#include "main.hpp"

extern "C" void redis_connect(int, char **);
extern "C" void redis_send(char *, char);
extern "C" void redis_receive(char *, char);

#define MAXSTR 512
#define MAXINT 16

int MAXDEPTH = 6;  //< Minimax cutoff depth

char EVAL_USED = 'a';

std::ofstream graphStream("graph.dot");
gm::LogLine<gm::LogLvl::Warn> graphLine(graphStream);
gm::LogLine<gm::LogLvl::Info> logLine(std::clog);

char parse(Board & board, char *buf)
{
	std::cout << "> bufer is |" << buf << "|\n";
	std::cout << std::flush;

	char currentPlayer;
	char lastPlayer;

	// Parses received string
	sscanf(strtok(buf, " \n"), "%c", &currentPlayer);
	sscanf(strtok(NULL, " "), "%c", &lastPlayer);
	std::cout << "Last move was |" << strtok(NULL, "\n") << "|\n";

	board.geeseNo_ = 0; // Reset geese counter
	for(int i = 0; i < 9; i++)
	{
			strncpy(board.map[i], strtok(NULL, "\n"), 10);
			for (int j = 0; j < 10; j++) {
				if (board.map[i][j] == '#')
					board.map[i][j] = piece::oob; // Remove trash
				if (board.map[i][j] == piece::goose)
					++board.geeseNo_; // Update geeseNo
				if (board.map[i][j] == piece::fox)
					board.foxPos_ = Coord{i, j}; // Update foxPos_
			}
	}

	std::cout << "> Updated board from server; foxPos:" << board.foxPos_ << "; geeseNo=" << board.geeseNo_ << "\n" << board;

	std::cout << std::flush;

	return currentPlayer;
}

int main(int argc, char **argv)
{
	if(argc < 2){
		std::cout << "Usage: " << argv[0] << " <r ou g> <ip (opt)> <porta (opt)> <maxdepth (opt)>" << std::endl;
		std::cout << "   ip: parâmetro opcional que indica o ip ou o hostname do servidor redis\n";
		std::cout << "       o valor default é 127.0.0.1\n";
		std::cout << "   porta: parâmetro opcional que indica a porta do servidor redis\n";
		std::cout << "          o valor default é 10001\n";
		std::cout << "   maxdepth: profundidade máxima da árvore de busca\n";
		std::cout << "	 heurística: 'a' ou 'b' (padrão 'a', ver leiame.txt)";
		std::cout << std::endl;
		return 1;
	}

	char thisPlayer = argv[1][0];
	Board board;

	if(argc > 4)
		MAXDEPTH = atoi(argv[4]);

	if(argc > 5)
		EVAL_USED = argv[5][0];

	char buffer[MAXSTR];

	// Connects with the game controller
	redis_connect(argc, argv);

	int turns = 0;

	while (1)
	{
		std::cout << ">> Waiting for answer..." << std::endl;
		redis_receive(buffer, thisPlayer);
		// Interpret server answer
		char nextPlayer = parse(board, buffer);
		// If it's my turn
		if(nextPlayer == thisPlayer)
		{
			std::cout << "# My turn!" << std::endl;

			graphLine << "digraph " << turns << "{\n";

			// Decide best move with minimax
			MoveUtility moveUtil; //< best move found and it's utility
			if (thisPlayer == piece::fox) {
				moveUtil = minimax<MinPlayer>(board, MINUSINF, PLUSINF, 0);
			} else {
				moveUtil = minimax<MaxPlayer>(board, MINUSINF, PLUSINF, 0);
			}

			// Build string and send move to redis
			sprintf(buffer, "%s",(moveUtil.move.tostring()).c_str());
			
			std::cout << "<< Sending move: '" << buffer << "' Util=" << moveUtil.utility << std::endl;

			redis_send(buffer, thisPlayer);

			graphLine << "}" << std::endl;
		}

		++turns;
	}

	return 0;
}
